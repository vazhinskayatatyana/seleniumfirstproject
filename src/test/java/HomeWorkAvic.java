import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


public class HomeWorkAvic {

    private WebDriver driver;

    @BeforeTest
    public void profileSetUp() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
    }

    @BeforeMethod
    public void testsSetUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://avic.ua/");
    }

    @Test(priority = 1)
    public void checkThatUrlContainsSelected() {
        driver.findElement(By.xpath("//a[contains (text(),'Уцененные товары')]")).click();
        assertTrue(driver.getCurrentUrl().contains("discount"));
    }

    @Test(priority = 2)
    public void checkElementsAmountOnSearchPage() {
        driver.findElement(By.xpath("//span[contains (text(),'Смартфоны и телефоны')]")).click();
        driver.findElement(By.xpath("//div[@class = 'brand-box__title']/a[@href='https://avic.ua/smartfonyi' and contains (text(), 'Смартфоны')]")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        List<WebElement> elementsList = driver.findElements(By.xpath("//div[@class='prod-cart__descr']"));
        int actualElementsSize = elementsList.size();
        assertEquals(actualElementsSize, 12);
    }

    @Test(priority = 3)
    public void checkAddToCart() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(60));

        driver.findElement(By.xpath("//span[contains (text(),'Смартфоны и телефоны')]")).click();
        driver.findElement(By.xpath("//div[@class = 'brand-box__title']/a[@href='https://avic.ua/smartfonyi' and contains (text(), 'Смартфоны')]")).click();

        wait.until(
                webDriver -> ((JavascriptExecutor) webDriver)
                        .executeScript("return document.readyState")
                        .equals("complete")
        );
        driver.findElement(By.xpath("//a[@class='prod-cart__buy'][contains(@data-ecomm-cart,'Google Pixel 6 8')]")).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("js_cart")));
        String actualProductInCart =
                driver.findElement(By.xpath("//span[contains (text(),'Pixel 6 8/256GB')]")).getText();
        assertEquals(actualProductInCart, "Смартфон Google Pixel 6 8/256GB Stormy Black");

    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }
}


